export default {
  isAuthenticated (state, commit, rootState) {
    return rootState.auth.loggedIn
  }
}
