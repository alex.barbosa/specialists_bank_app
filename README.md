# Subir a aplicação na máquina local

1. Fazer o clone do repositório no gitLab `https://gitlab.com/alex.barbosa/specialists_bank_app.git`

# Subir a aplicação na máquina local

2. Instalar as dependências

```bash
$ yarn install
```
3. Rodar a aplicação na máquina

```bash
$ yarn dev
```

# Subir o container Docker

```bash
$ docker compose up --build
```
ou
```bash
$ docker-compose up --build
```

# Acesar a aplicação pelo ip da máquina local ou:

`http;//localhost.com` / `http;//127.00.0.1.com`
