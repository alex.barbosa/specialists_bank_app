import { createLocalVue, shallowMount } from '@vue/test-utils'
import IndexPage from '@/pages/index.vue'

const stubs = {
  VRow: {
    name: 'v-row',
    template: '<div class="v-row-stub"></div>'
  }
}

describe('IndexPage', () => {
  const localVue = createLocalVue()

  it('is a Vue instance', () => {
    const wrapper = mountWrapper(localVue, stubs)
    expect(wrapper.vm).toBeTruthy()
  })
})

function mountWrapper (localVue, stubs) {
  return shallowMount(IndexPage, {
    localVue,
    stubs
  })
}
