import { createLocalVue, shallowMount } from '@vue/test-utils'
import UsersPage from '@/pages/users/index.vue'

const stubs = {
  vRow: {
    name: 'row',
    template: '<div class="div-row-stub"></div>'
  }
}

describe('UsersPage', () => {
  const localVue = createLocalVue()
  const wrapper = mountWrapper(localVue, stubs)
  it('is a Vue instance', () => {
    expect(wrapper.vm).toBeTruthy()
  })
})

function mountWrapper (localVue, stubs) {
  return shallowMount(UsersPage, {
    localVue,
    stubs
  })
}
