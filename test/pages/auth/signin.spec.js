import { createLocalVue, shallowMount } from '@vue/test-utils'
import SignInPage from '@/pages/auth/signin.vue'

const stubs = {
  vContainer: {
    name: 'container',
    template: '<div class="div-container-stub"></div>'
  }
}

describe('SignInPage', () => {
  const localVue = createLocalVue()
  const wrapper = mountWrapper(localVue, stubs)
  it('is a Vue instance', () => {
    expect(wrapper.vm).toBeTruthy()
  })
})

describe('methods', () => {
  const localVue = createLocalVue()
  const wrapper = mountWrapper(localVue, stubs)
  it('#openEerrorMessage', () => {
    wrapper.vm.errorMessage = false
    expect(wrapper.vm.errorMessage).toBeFalsy()
  })
})

function mountWrapper (localVue, stubs) {
  return shallowMount(SignInPage, {
    localVue,
    stubs
  })
}
